import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) {
        System.out.println("Insert dimnesion of table");
        int dim = new Scanner(System.in).nextInt();
        char[][] board = new char[dim][dim];

        System.out.println(dim);
        printBoard(board); //tutaj w main, drukuje stworzoną tablice uzywajac zdefiniowanej wczesniej metody statycznej
    }

    private static void printBoard(char[][] board) {
        int dim = board.length; // przypisujepodany  rozmiar tablicy do dlugosci tej tablicy
        System.out.print("\t"); // wykonuję tabulację na początku, opcją print - nie println, zeby wszystko bylo obok siebie, nie w nowej linii
        for (int i = 0; i < board.length; i++) {  //drukuje naglowki kolumn
            System.out.print(i + "\t"); //jezeli dam tutaj println - to naglowki wydrukuję jeden pod drugim
            // teraz przeskakuje do kolejnej linii i zaczynam drukować nagłówki
        }

        System.out.println();
        // System.out.println(); nie moge dac tu osobnej linii, bo wszystko wydrukuje sie 1 pod drugim, dam ja poza petla tworzoac nowy rzadek

        for (int row = 0; row < board.length; row++) {
            System.out.print(row + ":\t"); //pierwsza petla drukujr naglowki wierszy

            for (int column = 0; column < board.length ; column ++){ // i od razu po wydrukowaniu kazdego naglowka drukuje dla niego  kolumny
                System.out.print(column + "\t");}
            System.out.println();

        }

    }
}
